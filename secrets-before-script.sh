#! /usr/bin/env bash
set -e
default_access_token_res=$(
    curl --fail-with-body -s -S -H "Metadata-Flavor: Google" \
        "http://metadata.google.internal/computeMetadata/v1/instance/service-accounts/default/token"
)
access_token=$(echo "$default_access_token_res" | jq -r ".access_token")

if [[ -n "$GOOGLE_IMPERSONATE_SERVICE_ACCOUNT" ]]; then
    echo "Impersonating the $GOOGLE_IMPERSONATE_SERVICE_ACCOUNT service account..."
    access_token_res=$(
        curl --fail-with-body -s -S -X POST \
        -H "Authorization: Bearer $access_token" \
        -H "Content-Type: application/json; charset=utf-8" \
        "https://iamcredentials.googleapis.com/v1/projects/-/serviceAccounts/${GOOGLE_IMPERSONATE_SERVICE_ACCOUNT/@/%40}:generateAccessToken" \
        -d '{"scope": ["https://www.googleapis.com/auth/cloud-platform"]}'
    )
    access_token=$(echo "$access_token_res" | jq -r ".accessToken")
fi

for secret_var in "${!GET_GCP_SECRET_@}"; do
    var_name_to_export="${secret_var/GET_GCP_SECRET_/}"
    secret_var_value="${!secret_var}"

    if [[ $secret_var_value == projects/* ]]; then
        secret_id="$secret_var_value"
    else
        IFS=':' read -ra secret_details <<< "$secret_var_value"
        secret_id="projects/${secret_details[0]}/secrets/${secret_details[1]}/versions/${secret_details[2]}"
    fi
    
    echo "Retrieving secret value from $secret_id"
    secret_res=$(
        curl --fail-with-body -s -S -H "Authorization: Bearer $access_token" \
            -H "Content-Type: application/json" \
            "https://secretmanager.googleapis.com/v1/$secret_id:access"
    )
    secret_data=$(echo "$secret_res" | jq -r ".payload.data" | base64 -d)
    export "$var_name_to_export=$secret_data"
done
