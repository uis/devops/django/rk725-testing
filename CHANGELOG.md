# Changelog

## [0.3.0](https://gitlab.developers.cam.ac.uk/uis/devops/django/rk725-testing/compare/0.2.0...0.3.0) (2024-05-09)


### Features

* release ([dfb472d](https://gitlab.developers.cam.ac.uk/uis/devops/django/rk725-testing/commit/dfb472d27f69326801788ca42fd64d0222768750))
* testing ([6f75f24](https://gitlab.developers.cam.ac.uk/uis/devops/django/rk725-testing/commit/6f75f24e04d4bb160ed37eb279670be8191c6093))

## [0.2.0](https://gitlab.developers.cam.ac.uk/uis/devops/django/rk725-testing/compare/0.1.0...0.2.0) (2024-05-09)


### Features

* test ([5270f17](https://gitlab.developers.cam.ac.uk/uis/devops/django/rk725-testing/commit/5270f1714258785b9c64eda14bf0bcee38e842c1))

## 0.1.0 (2024-05-08)


### Features

* initial testing ([3dd546f](https://gitlab.developers.cam.ac.uk/uis/devops/django/rk725-testing/commit/3dd546f986014769b3415254dcd734b1e63ab288))
* release ([3ea8fd8](https://gitlab.developers.cam.ac.uk/uis/devops/django/rk725-testing/commit/3ea8fd881ff676007856289c2df631eb6f2cb3a8))
* testing ([f47fcc7](https://gitlab.developers.cam.ac.uk/uis/devops/django/rk725-testing/commit/f47fcc7d3dbb45686e82cc59f5fd76e00ca88d7d))
* yay ([b244262](https://gitlab.developers.cam.ac.uk/uis/devops/django/rk725-testing/commit/b2442627bbaa8e5cf3e8169adf870ddb58e94477))
* yay ([c776cb6](https://gitlab.developers.cam.ac.uk/uis/devops/django/rk725-testing/commit/c776cb65cb0a873e60934a68a3387ccc0dc8af33))
